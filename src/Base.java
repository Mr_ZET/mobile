import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.FileNotFoundException;

/**
 * Created by zet on 28.04.17.
 */
public class Base {

    private ChromeDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/home/zet/Downloads/chromedriver");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://mobile.de/");
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void getLinks() throws InterruptedException, FileNotFoundException {
        String mark = "Opel";
        String model = "Astra";
        HomePage home = new HomePage(driver, mark, model);
        String[][] links = home.getUrlsAndImagesMobile();
        SendMail send = new SendMail(links);
    }
}
