import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by zet on 05.05.17.
 */
public class Selenium {
    WebDriver driver;

    public Selenium(ChromeDriver driver){
        this.driver = driver;
    }
    /*
    * @select = selector of dropdown
    * @value = value of dropdown.
    */

    public void DropSelect(String select, String value){
        WebElement drop = driver.findElement(By.id(select));
        List<WebElement> options = drop.findElements(By.tagName("option"));
        for (WebElement option: options){
            if(value.equals(option.getText().trim()))
                option.click();
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }
    }
}
