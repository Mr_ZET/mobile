import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by zet on 28.04.17.
 */
public class HomePage {

    private WebDriver driver;

    public HomePage(ChromeDriver driver, String mark, String model) throws InterruptedException {
        this.driver = driver;
        Selenium drop_click = new Selenium(driver);
        drop_click.DropSelect("qsmake", mark);
        drop_click.DropSelect("qsmodel", model);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("qsfrg")).sendKeys("2010");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("qsdet")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        drop_click.DropSelect("emissionClass-ds","Euro5");
        driver.findElement(By.id("withImage-true-ds")).sendKeys(Keys.SPACE);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("dsp-upper-search-btn")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        drop_click.DropSelect("so-sb","Preis aufsteigend");
    }

    public String[][] getUrlsAndImagesMobile() {
        List<WebElement> results = driver.findElements(By.cssSelector("div.cBox-body.cBox-body--resultitem.rbt-reg.rbt-no-top"));
        String[][] res = new String[results.size() + 1][results.size() + 1];
        for (int i = 1; i < results.size(); i++) {
            res[i][1] = driver.findElement(By.xpath("//div[@class='cBox-body cBox-body--resultitem rbt-reg rbt-no-top'][" + i + "]/a")).getAttribute("href");
            res[i][2] = driver.findElement(By.xpath("//div[@class='cBox-body cBox-body--resultitem rbt-reg rbt-no-top'][" + i + "]//img")).getAttribute("src");
            res[i][3] = driver.findElement(By.xpath("//div[@class='cBox-body cBox-body--resultitem rbt-reg rbt-no-top'][" + i + "]//span[@class='h3 u-block']")).getText();
        }
        return res;
    }
}